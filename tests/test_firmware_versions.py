import pytest

from sangaboard import Sangaboard
from sangaboard.extensible_serial_instrument import DummySerialDevice

def dummy_sangaboard_port():
    """Create a dummy port, with the minimal responses to work

    When the Sangaboard class is initialised, it checks the board,
    the version, and the optional modules.  All of those commands
    must therefore return reasonable values.
    """
    port = DummySerialDevice()
    port.register_response("version", "Sangaboard Firmware v0.5.1\r\n")
    port.register_response("list_modules", "--END--\r\n")
    port.register_response("board", "Sangaboard v0.3\r\n")
    port.print_buffers = True
    return port

def test_version_0_5():
    port = dummy_sangaboard_port()
    port.replace_response("version", "Sangaboard Firmware v0.5\r\n")
    sb = Sangaboard(port)
    assert sb.firmware_version == '0.5'
    assert sb.version_tuple == (0, 5)

def test_version_0_5_1():
    port = dummy_sangaboard_port()
    port.replace_response("version", "Sangaboard Firmware v0.5.1\r\n")
    sb = Sangaboard(port)
    assert sb.firmware_version == '0.5.1'
    assert sb.version_tuple == (0, 5)

def test_version_0_5_1_custom():
    port = dummy_sangaboard_port()
    port.replace_response("version", "Sangaboard Firmware v0.5.1-custom\r\n")
    sb = Sangaboard(port)
    assert sb.firmware_version == '0.5.1-custom'
    assert sb.version_tuple == (0, 5)

def test_version_0_6_0():
    port = dummy_sangaboard_port()
    port.replace_response("version", "Sangaboard Firmware v0.6.0\r\n")
    with pytest.raises(IOError):
        Sangaboard(port)

def test_version_0_3():
    port = dummy_sangaboard_port()
    port.replace_response("version", "OpenFlexure Motor Driver Firmware v0.3\r\n")
    with pytest.raises(IOError):
        Sangaboard(port)
